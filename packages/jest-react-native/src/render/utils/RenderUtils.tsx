/* eslint-disable jest/expect-expect,jest/valid-title,jest/no-export,import/newline-after-import,import/first */

import * as React from 'react';

export * from '@testing-library/react-native';
import {ReactTestInstance} from 'react-test-renderer';
import {render as renderOriginal} from '@testing-library/react-native';

export type RenderWrap = (props: {children: React.ReactElement}) => React.ReactElement;
export type Options = {wrapper?: 'navigation' | 'navigationNative'};

export function render<T>(
  wrap: RenderWrap,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: any,
  initialParams?: T,
  options?: Options,
) {
  let children: React.ReactElement;
  if (options?.wrapper === 'navigation') {
    const {
      default: MockedNavigator,
    } = require('@spryrocks/jest-react-native-mock/lib/react-navigation/MockedNavigator');
    children = <MockedNavigator component={component} initialParams={initialParams} />;
  } else if (options?.wrapper === 'navigationNative') {
    const {
      default: MockedNavigatorNative,
    } = require('@spryrocks/jest-react-native-mock/lib/react-navigation/MockedNavigatorNative');
    children = (
      <MockedNavigatorNative component={component} initialParams={initialParams} />
    );
  } else {
    children = React.createElement(component, initialParams);
  }

  const renderedComponent = renderOriginal(
    wrap({
      children,
    }),
  );
  return {...renderedComponent};
}

export const getAllText = (element: ReactTestInstance): string => {
  let text: string = '';
  for (const childElement of element.children) {
    if (typeof childElement === 'string') {
      text += childElement;
    } else {
      text += getAllText(childElement);
    }
  }
  return text;
};
