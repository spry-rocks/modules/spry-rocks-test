/* eslint-disable jest/no-export,jest/valid-title,jest/expect-expect,import/newline-after-import,import/first,import/no-extraneous-dependencies */
import SagaTester, {SagaFunction} from 'redux-saga-tester';
import {test} from '@jest/globals';

const testSagaInternal = <State, TestState extends Partial<State>, TSetupResult>(
  name: string,
  {
    saga,
    initialState,
  }: {
    saga: SagaFunction;
    initialState: TestState;
  },
  setup: (utils: {sagaTester: SagaTester<TestState>}) => Promise<TSetupResult>,
  check: (utils: {
    sagaTester: SagaTester<TestState>;
    setupResult: TSetupResult;
  }) => Promise<void>,
) =>
  test(name, async () => {
    const sagaTester = new SagaTester<TestState>({
      initialState,
    });
    const task = sagaTester.start(saga);

    const setupResult = await setup({sagaTester});

    await task.done;

    await check({sagaTester, setupResult});

    if (task.error()) {
      throw task.error();
    }
  });

export type TestSagaOptions<Dependencies, TestState> = {
  initialState: TestState;
  setupDependencies: () => Promise<Dependencies>;
  saga: SagaFunction;
};
export type TestSagaSetup<State, Dependencies, AdditionalDependencies> = (
  utils: {sagaTester: SagaTester<Partial<State>>} & Dependencies,
) => AdditionalDependencies;
export type TestSagaCheck<State, Dependencies, AdditionalDependencies> = (
  utils: {sagaTester: SagaTester<Partial<State>>} & Dependencies & AdditionalDependencies,
) => Promise<void>;

export const testSaga = <
  State,
  Dependencies,
  AdditionalDependencies,
  TestState extends Partial<State>
>(
  name: string,
  {initialState, setupDependencies, saga}: TestSagaOptions<Dependencies, TestState>,
  setup: TestSagaSetup<State, Dependencies, AdditionalDependencies>,
  check: TestSagaCheck<State, Dependencies, AdditionalDependencies>,
) =>
  testSagaInternal(
    name,
    {saga, initialState},
    async ({sagaTester}) => {
      const dependencies = await setupDependencies();
      const additionalDependencies = setup({sagaTester, ...dependencies});
      return {dependencies, additionalDependencies};
    },
    async ({sagaTester, setupResult: {dependencies, additionalDependencies}}) => {
      await check({sagaTester, ...dependencies, ...additionalDependencies});
    },
  );
