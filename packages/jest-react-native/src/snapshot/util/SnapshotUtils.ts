/* eslint-disable jest/no-export,jest/valid-title */
import {Options, render, RenderWrap} from '../../render/utils/RenderUtils';
import React from 'react';

export const testSnapshot = <State, InitialParams>(
  testName: string,
  wrap: RenderWrap,
  setupDependencies: () => Promise<void>,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: React.ComponentType<any>,
  initialParams?: InitialParams,
  options?: {initialState: Partial<State>} & Options,
) =>
  test(testName, async () => {
    await setupDependencies();
    const result = render(wrap, component, initialParams, options);
    expect(result.toJSON()).toMatchSnapshot();
  });
