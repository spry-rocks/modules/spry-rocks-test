/* eslint-disable import/first */
import {jest} from '@jest/globals';
jest.mock('@bugsnag/react-native');
import Bugsnag from '@bugsnag/react-native';
import {mocked} from 'jest-mock';

const mock: typeof Bugsnag = mocked(Bugsnag);

export default mock;
