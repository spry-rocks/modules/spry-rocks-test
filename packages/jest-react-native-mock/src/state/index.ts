import configureStore from 'redux-mock-store';

const createMockStore = <State>(initialState: Partial<State>) => {
  const mockStore = configureStore<Partial<State>>();

  return mockStore(initialState);
};

export {createMockStore};
