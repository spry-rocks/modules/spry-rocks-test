import {jest} from '@jest/globals';
import {Platform} from 'react-native';

export type OS = typeof Platform.OS;

export default {
  setOS: (mockOS: OS) => {
    Platform.OS = mockOS;
    Platform.select = jest.fn((dict) => dict[mockOS]);
  },
};
