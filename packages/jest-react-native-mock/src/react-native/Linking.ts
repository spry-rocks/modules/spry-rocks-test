/* eslint-disable import/newline-after-import,import/first */
import {jest} from '@jest/globals';
import {mocked} from 'jest-mock';
jest.mock('react-native/Libraries/Linking/Linking', () => ({
  canOpenURL: jest.fn(),
  openURL: jest.fn(),
}));
import {Linking} from 'react-native';

export default mocked(Linking, true);
