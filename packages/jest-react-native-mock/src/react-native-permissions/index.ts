/* eslint-disable @typescript-eslint/ban-ts-comment,import/first,import/newline-after-import */
// @ts-ignore
import mock from 'react-native-permissions/mock';
import PERMISSIONS from './PERMISSIONS';
const mockPERMISSIONS = PERMISSIONS;
import {jest} from '@jest/globals';
jest.mock('react-native-permissions', () => ({
  ...mock,
  PERMISSIONS: mockPERMISSIONS,
}));
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import {request, requestMultiple} from 'react-native-permissions';
import {mocked} from 'jest-mock';

export default {
  //
  request: mocked(request, false),
  requestMultiple: mocked(requestMultiple, false),
};
