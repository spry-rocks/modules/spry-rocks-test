/* eslint-disable import/first */
import {jest} from '@jest/globals';
jest.mock('react-native-localize', () => ({}));
import {mocked} from 'jest-mock';
import RNLocalize from 'react-native-localize';

export default mocked(RNLocalize, true);
