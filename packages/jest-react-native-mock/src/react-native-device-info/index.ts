/* eslint-disable @typescript-eslint/ban-ts-comment,import/newline-after-import,import/first */
// @ts-ignore
const mockImpl = require('react-native-device-info/jest/react-native-device-info-mock');
import {jest} from '@jest/globals';
jest.mock('react-native-device-info', () => mockImpl);
import inst from 'react-native-device-info';

export default inst;
