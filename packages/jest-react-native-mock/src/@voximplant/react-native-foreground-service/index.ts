/* eslint-disable import/first */
import {jest} from '@jest/globals';
jest.mock('@voximplant/react-native-foreground-service');
import {mocked} from 'jest-mock';
import VIForegroundService from '@voximplant/react-native-foreground-service';

export default mocked(VIForegroundService);

export type {
  NotificationConfig,
  NotificationChannelConfig,
} from '@voximplant/react-native-foreground-service';
