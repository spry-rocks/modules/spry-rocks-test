/* eslint-disable import/newline-after-import,import/first */
import {jest} from '@jest/globals';
import {mocked} from 'jest-mock';
jest.mock('react-native-disable-battery-optimizations-android', () => ({
  isBatteryOptimizationEnabled: jest.fn(),
  openBatteryModal: jest.fn(),
}));
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import RNDisableBatteryOptimizationsAndroid from 'react-native-disable-battery-optimizations-android';

export default {
  isBatteryOptimizationEnabled: mocked<() => Promise<boolean>>(
    RNDisableBatteryOptimizationsAndroid.isBatteryOptimizationEnabled,
    false,
  ),
  openBatteryModal: mocked<() => Promise<void>>(
    RNDisableBatteryOptimizationsAndroid.openBatteryModal,
    false,
  ),
};
