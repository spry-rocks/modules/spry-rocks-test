/* eslint-disable import/first */
import {jest} from '@jest/globals';
jest.mock('@react-native-community/async-storage-backend-legacy');
import asyncStorageBackendLegacy from '@react-native-community/async-storage-backend-legacy';
import {mocked} from 'jest-mock';

export default mocked(asyncStorageBackendLegacy);
