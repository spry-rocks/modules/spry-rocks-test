/* eslint-disable @typescript-eslint/no-unused-vars */
import * as TypeMoq from 'typemoq';
import {
  ILocalizationService,
  LanguageChangedCallback,
} from '@spryrocks/localization-react-native';

class MockImpl extends ILocalizationService {
  addOnLanguageChanged(callback: LanguageChangedCallback): void {
    throw new Error('Not implemented');
  }

  initialize(): Promise<void> {
    throw new Error('Not implemented');
  }

  removeOnLanguageChanged(callback: LanguageChangedCallback): void {
    throw new Error('Not implemented');
  }

  setLanguage(lang: string): Promise<void> {
    throw new Error('Not implemented');
  }
}

export type ILocalizationServiceMock = TypeMoq.IMock<ILocalizationService>;

export const createLocalizationServiceMock = (): ILocalizationServiceMock =>
  TypeMoq.Mock.ofType(MockImpl);
