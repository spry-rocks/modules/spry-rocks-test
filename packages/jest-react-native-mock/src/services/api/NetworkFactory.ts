/* eslint-disable @typescript-eslint/no-unused-vars */
import {IRestNetwork, IRestNetworkFactory} from '@spryrocks/react-api';

class RestNetworkFactory implements IRestNetworkFactory {
  constructor(private readonly network: IRestNetwork) {}

  createNetwork(baseURL: string, headers: object, getHeaders: () => object) {
    return this.network;
  }
}

export const createRestNetworkFactory = (network: IRestNetwork): IRestNetworkFactory =>
  new RestNetworkFactory(network);
