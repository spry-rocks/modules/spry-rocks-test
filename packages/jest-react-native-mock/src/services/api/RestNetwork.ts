/* eslint-disable @typescript-eslint/no-unused-vars,class-methods-use-this,@typescript-eslint/no-explicit-any,import/no-extraneous-dependencies */
import * as TypeMoq from 'typemoq';
import {
  DownloadFileConfig,
  DownloadFileResponse,
} from '@spryrocks/react-api/lib/rest/INetwork';
import {AxiosRequestConfig} from 'axios';
import {IRestNetwork} from '@spryrocks/react-api';

class RestNetworkMockImpl implements IRestNetwork {
  delete(url: string, config?: AxiosRequestConfig): Promise<void> {
    throw new Error('Not implemented');
  }

  get<T>(url: string, config?: AxiosRequestConfig): Promise<T> {
    throw new Error('Not implemented');
  }

  head(url: string, config?: AxiosRequestConfig): Promise<void> {
    throw new Error('Not implemented');
  }

  patch<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    throw new Error('Not implemented');
  }

  post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    throw new Error('Not implemented');
  }

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  uploadFile<T>(url: string, file: string | File, name: string): Promise<T> {
    throw new Error('Not implemented');
  }

  put<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<T> {
    throw new Error('Not implemented');
  }

  request<T>(config: AxiosRequestConfig): Promise<T> {
    throw new Error('Not implemented');
  }

  downloadFile(
    url: string,
    data?: any,
    config?: DownloadFileConfig,
  ): Promise<DownloadFileResponse> {
    throw new Error('Not implemented');
  }
}

export type IRestNetworkMock = TypeMoq.IMock<IRestNetwork>;

export const createRestNetworkMock = (): IRestNetworkMock =>
  TypeMoq.Mock.ofType(RestNetworkMockImpl);
