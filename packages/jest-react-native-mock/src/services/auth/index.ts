/* eslint-disable @typescript-eslint/no-unused-vars,class-methods-use-this */
import * as TypeMoq from 'typemoq';
import {Authentication, AuthenticationOptions, Listener, IAuthInfoKeeper} from '@spryrocks/react-auth';

class MockImpl extends IAuthInfoKeeper<never, never> {
  addListener(listener: Listener<never, never>) {
    throw new Error('Not implemented');
  }

  authenticate(
    session: never,
    user: never,
    options?: AuthenticationOptions,
  ): Promise<void> {
    throw new Error('Not implemented');
  }

  getAuthentication(): Authentication<never, never> | undefined {
    throw new Error('Not implemented');
  }

  getSession(): never | undefined {
    throw new Error('Not implemented');
  }

  getUser(): never | undefined {
    throw new Error('Not implemented');
  }

  initialize(): Promise<void> {
    throw new Error('Not implemented');
  }

  isAuthenticated(): boolean {
    throw new Error('Not implemented');
  }

  removeListener(listener: Listener<never, never>) {
    throw new Error('Not implemented');
  }

  reset(): Promise<void> {
    throw new Error('Not implemented');
  }

  updateSession(session: never): Promise<void> {
    throw new Error('Not implemented');
  }

  updateUser(user: never): Promise<void> {
    throw new Error('Not implemented');
  }
}

export type IAuthInfoKeeperMock<T extends IAuthInfoKeeper<unknown, unknown>> = TypeMoq.IMock<T>;

export const createAuthInfoKeeperMock = <T extends IAuthInfoKeeper<unknown, unknown>>(): IAuthInfoKeeperMock<T> =>
  TypeMoq.Mock.ofType(MockImpl) as unknown as IAuthInfoKeeperMock<T>;
