/* eslint-disable class-methods-use-this */
import * as TypeMoq from 'typemoq';
import {
  IPermissionsService,
  RequestLocationPermissionsOptions,
  RequestLocationPermissionsResult,
  RequestPermissionResult,
} from '@spryrocks/permissions-react-native';

class MockImpl extends IPermissionsService {
  requestLocationPermissions(
    _options?: RequestLocationPermissionsOptions,
  ): Promise<RequestLocationPermissionsResult> {
    throw new Error('Not implemented');
  }

  requestMediaPermission(): Promise<RequestPermissionResult> {
    throw new Error('Not implemented');
  }

  openSettings(): Promise<void> {
    throw new Error('Not implemented');
  }
}

export type IPermissionsServiceMock = TypeMoq.IMock<IPermissionsService>;

export const createPermissionsServiceMock = (): IPermissionsServiceMock =>
  TypeMoq.Mock.ofType(MockImpl);
