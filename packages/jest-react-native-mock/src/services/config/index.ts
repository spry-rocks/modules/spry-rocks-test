/* eslint-disable class-methods-use-this,@typescript-eslint/no-unused-vars */
import * as TypeMoq from 'typemoq';
import {IConfigService} from '@spryrocks/config-react';

class MockImpl extends IConfigService {
  getNumberOptional(key: string): number | undefined {
    throw new Error('Not implemented');
  }

  getBooleanOptional(key: string): boolean | undefined {
    throw new Error('Not implemented');
  }

  get(key: string, defaultValue?: string): string {
    throw new Error('Not implemented');
  }

  getBoolean(key: string, defaultValue?: boolean): boolean {
    throw new Error('Not implemented');
  }

  getNumber(key: string, defaultValue?: number): number {
    throw new Error('Not implemented');
  }

  getOptional(key: string): string | undefined {
    throw new Error('Not implemented');
  }
}

export type IConfigServiceMock = TypeMoq.IMock<IConfigService>;

export const createConfigServiceMock = (): IConfigServiceMock =>
  TypeMoq.Mock.ofType(MockImpl);
