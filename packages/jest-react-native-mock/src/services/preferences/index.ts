/* eslint-disable class-methods-use-this,@typescript-eslint/no-unused-vars */
import * as TypeMoq from 'typemoq';
import {IPreferencesService} from '@spryrocks/preferences-react-native';
import {ParamListBase} from '@spryrocks/preferences-react-native/lib/IPreferencesService';

class MockImpl<ParamList extends ParamListBase> extends IPreferencesService<ParamList> {
  get<Key extends keyof ParamList>(key: Key): Promise<ParamList[Key] | undefined> {
    throw new Error('Not implemented');
  }

  set<Key extends keyof ParamList>(
    key: Key,
    value: ParamList[Key] | undefined,
  ): Promise<void> {
    throw new Error('Not implemented');
  }
}

export type IPreferencesServiceMock<ParamList extends ParamListBase> = TypeMoq.IMock<
  IPreferencesService<ParamList>
>;

export const createPreferencesServiceMock = <
  ParamList extends ParamListBase
>(): IPreferencesServiceMock<ParamList> =>
  TypeMoq.Mock.ofType<IPreferencesService<ParamList>>(MockImpl);
