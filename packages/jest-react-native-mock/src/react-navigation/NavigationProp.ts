import * as TypeMoq from 'typemoq';
import {NavigationProp} from '@react-navigation/native';
import {ParamListBase} from '@react-navigation/routers';

export type NavigationPropMock<ParamList extends ParamListBase> = TypeMoq.IMock<
  NavigationProp<ParamList>
>;

export const createNavigationPropMock = <
  ParamList extends ParamListBase
>(): NavigationPropMock<ParamList> => TypeMoq.Mock.ofType<NavigationProp<ParamList>>();
