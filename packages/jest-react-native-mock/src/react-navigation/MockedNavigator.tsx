/* eslint-disable @typescript-eslint/no-explicit-any */
import 'react-native-gesture-handler';
import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator<any>();

function MockedNavigator<T>({
  component,
  initialParams,
}: {
  component: React.ComponentType<T>;
  initialParams: T | undefined;
}) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="MockedScreen"
          component={component}
          initialParams={initialParams}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MockedNavigator;
